# checkin_update controller
class CheckinUpdatesController < ApplicationController
  def create
    @checkin = Checkin.find_by(id: params[:checkin_id])
    save_checkin_update_values if @checkin
    respond_to do |format|
       format.js { redirect_to checkin_checkin_activities_path(param) }
    end
  end

  private

  def checkin_update_params
    params.require(:checkin_update).permit(:description, :progress,
                                           :latitude, :longitude)
      .merge(employee_id: @current_employee.id,
             description: params[:description],
             previous_progress: params[:previous_progress])
  end

  def checkin_params
    params.require(:checkin_update).permit(:progress)
  end

  def save_checkin_update_values
    params[:previous_progress] = @checkin.progress
    @checkin.assign_attributes(checkin_params)
    @checkin.set_completed_at
    checkin_update = @checkin.checkin_updates.build(checkin_update_params)
    checkin_update.notify_on_create(current_employee) if @checkin.save
  end

  def param
   params.merge(update_list: true)
  end
end

