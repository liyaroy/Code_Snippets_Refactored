module Api
  module V1
    # actitvity controller
    class CheckinActivitiesController < Api::V1::BaseController
      def index
        sanitize_incoming_params(params)
        get_pagination_values(params)
        @checkin = Checkin.find_by(id: params[:checkin_id])
        fetch_activity_records if @checkin
        render json: activity_hash
      end

      private

      def fetch_activity_records
        @checkin_activities = @checkin.checkin_activities.includes(activity: :employee)
                              .find_records(params)
        set_pagination_header(@checkin_activities, params)
        params[:checkin] = @checkin
        params[:current_employee] = @current_employee
      end

      def set_pagination_header(checkin_activities, params)
        return nil unless checkin_activities
        # if checkin_activities.present?
        if params.has_key?(:page_activity_id)
          set_activity_primary_key_pagination_header(params, checkin_activities.last)
        else
          set_activity_pagination_header(params)
        end
        # end
      end

      def activity_hash
        { server_time: Time.now,
          checkin_activities: ActiveModel::ArraySerializer.new(@checkin_activities,
                                                               each_serializer: CheckinActivitySerializer,
                                                               params: params) }
      end
    end
  end
end

