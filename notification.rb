# Notification
class Notification
  attr_reader :emp_name, :checkin_id, :name, :delete_description, :method_name
  def initialize(args = {})
    args.each { |k, v| instance_variable_set("@#{k}", v) }
  end

  def notification_hash
    case method_name
    when 'create' then notification_create_hash
    when 'update' then notification_update_hash
    when 'import' then notification_import_hash
    when 'overdue' then notification_overdue_hash
    when 'delete' then notification_delete_hash
    end
  end

  def notification_create_hash
    { subject: "#{@emp_name} created checkin ##{@checkin_id}",
      content: "#{@emp_name} a checkin ##{@checkin_id}",
      link: true, event: 'create',
      title: "#{@emp_name.truncate(10)} created a new checkin" }
  end

  def notification_update_hash
    { subject: "#{@emp_name} edited checkin ##{@checkin_id}",
      content: "#{@emp_name} edited checkin ##{@checkin_id}",
      link: true, event: 'update',
      title: "#{@emp_name.truncate(10)} updated the checkin" }
  end

  def notification_import_hash
    { subject: "Created the checkin ##{@checkin_id} for #{@emp_name}",
      content: "Created the checkin ##{@checkin_id} for #{@emp_name}",
      link: true, event: 'update',
      title: '#checkin created for {@emp_name.truncate(10)}' }
  end

  def notification_delete_hash
    { subject: "#{@emp_name} deleted the checkin ##{@checkin_id}",
      content: "#{@emp_name} says:  " + "'#{@delete_description}'",
      link: false, event: 'delete',
      title: "#{@emp_name.truncate(10)} deleted the checkin" }
  end

  def notification_overdue_hash
    { subject: "Checkin is overdue - #{@emp_name}",
      content: "Checkin ##{@checkin_id} - #{@name} - assigned to #{@emp_name} is overdue!",
      link: true, event: 'checkin_overdue',
      title: "Checkin overdue - #{@emp_name.truncate(10)}" }
  end
end
